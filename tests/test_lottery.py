import pytest
from brownie import Lottery, accounts
from scripts.utils import *

def test_can_set_cost(lottery, owner):
    assert lottery.cost() == BASE_COST_OF_LOTTERY
    lottery.setCost("10 ether", {"from": owner})
    assert lottery.cost() == "10 ether"

def test_enter(lottery, owner, bob, alice):
    assert len(lottery.getPlayers()) == 0
    lottery.enter({"from": owner, "value": BASE_COST_OF_LOTTERY})
    assert len(lottery.getPlayers()) == 1

    lottery.enter({"from": bob, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    assert len(lottery.getPlayers()) == 3

def test_can_enter_multiple_times(lottery, alice):
    assert len(lottery.getPlayers()) == 0
    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    assert len(lottery.getPlayers()) == 1

    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    assert len(lottery.getPlayers()) == 3

def test_get_balance(lottery, owner, bob, alice):
    assert lottery.getBalance() / WEI_TO_ETHER == 0

    lottery.enter({"from": bob, "value": BASE_COST_OF_LOTTERY})

    assert lottery.getBalance() / WEI_TO_ETHER == .01

    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": owner, "value": BASE_COST_OF_LOTTERY})
    assert lottery.getBalance() / WEI_TO_ETHER == .03


def test_pick_winner(lottery, owner, bob, alice):
    lottery.enter({"from": bob, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": bob, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": owner, "value": BASE_COST_OF_LOTTERY})

    assert lottery.getBalance() / WEI_TO_ETHER == .04
    
    lottery.pickWinner()

    assert lottery.getBalance() / WEI_TO_ETHER == 0

    first_winner = lottery.getWinnerByLotteryId(1)
    assert first_winner in [alice, bob, owner]


def test_winner_gets_balance(lottery, owner, bob, alice, balance_to_address, address_to_account):
    lottery.enter({"from": bob, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": alice, "value": BASE_COST_OF_LOTTERY})
    lottery.enter({"from": owner, "value": BASE_COST_OF_LOTTERY})
    
    lottery.pickWinner()

    first_winner = lottery.getWinnerByLotteryId(1)
    assert first_winner in [alice, bob, owner]
    old_balance_plus_prize = balance_to_address[first_winner] + (0.02 * WEI_TO_ETHER)
    current_balance = address_to_account.get(first_winner).balance()
    assert  old_balance_plus_prize == current_balance




