import pytest
from brownie import Lottery, accounts
from scripts.utils import get_account


@pytest.fixture
def owner():
    return get_account()

@pytest.fixture
def bob():
    return accounts[1]

@pytest.fixture
def alice():
    return accounts[2]

@pytest.fixture
def lottery(owner):
    return Lottery.deploy({"from": owner})

@pytest.fixture
def balance_to_address(alice, bob, owner):
    return {
        alice : alice.balance(),
        bob : bob.balance(),
        owner : owner.balance()
    }

@pytest.fixture
def address_to_account(balance_to_address):
    return  {k.address: k for k in balance_to_address.keys()}

    