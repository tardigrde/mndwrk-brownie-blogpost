from brownie import accounts, Lottery
from scripts.utils import get_account, BASE_COST_OF_LOTTERY, WEI_TO_ETHER


def main():
    # deploy the contract
    lottery = Lottery.deploy({"from": get_account()})

    # enter with test accounts
    for i in range(10):
        lottery.enter({"from": accounts[i], "value": BASE_COST_OF_LOTTERY})
    
    # check if we have 10 players and enough balance
    assert lottery.getBalance() / WEI_TO_ETHER == .1
    assert len(lottery.getPlayers()) == 10

    print(f"Particpants: {lottery.getPlayers()}")
    print(f"Lottery balance: {lottery.getBalance()}")
    
    # pick winner
    lottery.pickWinner()

    # check if the lottery has been reset
    assert lottery.getBalance() == 0
    assert len(lottery.getPlayers()) == 0

    # get the winner
    winner = lottery.getWinnerByLotteryId(1)

    # check if the winner is from the accounts container
    assert winner in accounts

    print(f"The winner of the first lottery is: {winner}")

    


