from brownie import accounts, config, network, Contract, Lottery

WEI_TO_ETHER = 1000000000000000000

BASE_COST_OF_LOTTERY = ".01 ether"

def get_account():
    if network.show_active() == "development":
        return accounts[0]
    else:
        return accounts.add(config["wallets"]["from_key"])

def get_last_lotter_contract():
    return Lottery[-1]

def get_contract():
    try:
        return Contract(config['current']['contract']['address'])
    except Exception as e:
        print(f"Contract could be not looked up. Reason: {e}")
    return None