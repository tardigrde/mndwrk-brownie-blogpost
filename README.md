# Using brownie for Solidity Smart Contracts

This project is an appendix to a blogpost I wrote. It can be used as a starting point to learn `brownie` and its functionalities. 

It may help you:
- get your development environemnt configured,
- get an overview of a simple `brownie` project and its files,
- get started with testing and deployments.

Note: the setup was tested with Python v3.9 on Debian 11. For `Windows` users external docs will be linked.

## Clone the project

To clone this project you will need [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

Use https:

```shell
git clone https://codeberg.org/tardigrde/mndwrk-brownie-blogpost.git
```

or ssh:

```shell
git clone git@codeberg.org:tardigrde/mndwrk-brownie-blogpost.git
```

## Developer environment

### VSCode

#### Solidity Plugin

### Python env

#### pipx

Brownie recommends that you install it via [pipx](https://pypa.github.io/pipx/). Consult the docs on how to [install brownie](https://eth-brownie.readthedocs.io/en/stable/install.html).

Fow `Windows` users, probably this is the easier way.

#### pip in a virtualenv

With Python v3.9 we can just use `pip`. To create a new Python `virtualenv`, use the following commands (tested on Debian 11, should be applicable for most Linux distros):

```shell
python3 -m pip install virtualenv
virtualenv venv --python=python3.9
```

Then activate it, and install requirements:

```shell
source venv/bin/activate
pip install -r requirements.txt 
```

`Windows` users, please consult the virtualenv [installation docs](https://virtualenv.pypa.io/en/latest/installation.html), and [user guide](https://virtualenv.pypa.io/en/latest/user_guide.html).

At this point you have to make sure that `brownie` is now on your path, and you execute `brownie --help` from the terminal. 

Since this is a ready-made `brownie` project, you are now **all set up**. 

## Project setup

```
.
├── build               # contains generated build artifacts after you compiled the contract
├── contracts           # all the contracts relevant to this project
│   └── Lottery.sol
├── scripts             # contains utility and deployment scripts
│   ├── lottery.py
│   └── utils.py
└── tests               # pytests home directory
    ├── conftest.py
    └── test_lottery.py
```

## Contracts

The only contract in this project is [Lottery.sol](./contracts/Lottery.sol). It's a dead-simple Lottery application, where users can enter with some money and have a chance to win the game when it concludes.

It is inspired by *jspruance*'s' [block-explorer-tutorials/Lottery.sol](https://github.com/jspruance/block-explorer-tutorials/blob/main/smart-contracts/solidity/Lottery.sol) with some extra features and comments.

## Run the tests

Brownie uses [pytest](https://docs.pytest.org/) for testing. The tests can be found in the `tests` subdirectory.

Navigate to the project root in the terminal. If you use the `virtualenv`, activate the virtual environment, and run the tests:

```shell
brownie test
```

## Simulate lottery

The [lottery.py](./scripts/lottery.py) script simulates a lottery game.
To run it, use:

```shell
brownie run scripts/lottery.py
```

## Create a new brownie project

In order to create a new `brownie` project, the following command was used in an empty directory:

```shell
brownie init <path>
```

You could also use a ready-made template, also known as a `brownie mix`. Use the following command to learn more:

```shell
brownie bake --help
```
